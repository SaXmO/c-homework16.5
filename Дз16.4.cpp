﻿#include <iostream>
#include <time.h>
int main()
{
    const int N = 7;
    int array[N][N] = { {} };
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            array[i][j] = i + j;
            std::cout << array[i][j];
        }
        std::cout << "\n";
    }
    std::cout << "\n";
    {
        int SummArrayI = 0;
        int Ostatok = 0;
        struct tm buf;
        time_t t = time(NULL);
        localtime_s(&buf, &t);
        Ostatok = buf.tm_mday % N;
        std::cout << "Ostatok " << Ostatok << "\n";
        std::cout << "Day " << buf.tm_mday;
        std::cout << "\n" << "\n";
        for (int i = Ostatok; i < (Ostatok + 1); ++i)
            {
                for (int j = 0; j < N; ++j)
                {
                    SummArrayI = SummArrayI + array[i][j];
                    std::cout << SummArrayI << " ";
                }
            }
        std::cout << "\n";
        std::cout << "Summa stroki massiva " << SummArrayI;
    }

}
